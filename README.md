# Python Parser for CoMPASS Binary File

## Usage
- Include `pulseParser.py` in your project and add the following lines to your python script to use the parser.
    ```python
    from pulseParser import WaveBinFile     # import the parser to use it
    # your code follows...
    ```
- Documentation in folder `documentation`.
- Refer to `exampleUsage.py` for examples on using the parser.
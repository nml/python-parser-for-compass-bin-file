'''
Description: Test file for the paser.
Author: Ming Fang
Date: 2022-08-22 19:26:55
LastEditors: Ming Fang
LastEditTime: 2022-08-23 17:46:12
'''
import numpy as np
import unittest
from pulseParser import WaveBinFile


class TestBinParserV1(unittest.TestCase):
    def setUp(self):
        self.binFile = WaveBinFile('test_data_CoMPASSv1/test.bin', 1)

    def testGetCommonHeaderSuccess(self):
        self.assertEqual(self.binFile.versionNumber, 1)
        self.assertEqual(self.binFile.boardNumber, 0)
        self.assertEqual(self.binFile.channelNumber, 0)
        self.assertEqual(self.binFile._headersize, 24)
        self.assertEqual(self.binFile.numberOfSamplesPerPulse, 496)
        self.assertEqual(self.binFile.totalNumberOfPulses,
                         int(83279488 / (24 + 992)))

    def testGetFirstPulse(self):
        newPulse = self.binFile.readNextPulse()
        self.assertEqual(newPulse['Board'], 0)
        self.assertEqual(newPulse['Channel'], 0)
        self.assertEqual(newPulse['Time Stamp'], 241636409)
        self.assertEqual(newPulse['Energy'], 297)
        self.assertEqual(newPulse['Energy Short'], 153)
        self.assertEqual(newPulse['Flags'], 16384)
        self.assertEqual(newPulse['Number of Samples'], 496)

        samples = [
            2904, 2903, 2899, 2903, 2908, 2908, 2905, 2903, 2908, 2905, 2909,
            2905, 2907, 2911, 2904, 2903, 2902, 2909, 2904, 2906, 2904, 2900,
            2908, 2908, 2907, 2907, 2906, 2899, 2905, 2903, 2907, 2904, 2907,
            2907, 2903, 2907, 2908, 2904, 2905, 2901, 2904, 2904, 2905, 2909,
            2900, 2904, 2907, 2902, 3168, 3764, 4165, 4405, 4533, 4567, 4556,
            4511, 4448, 4374, 4292, 4218, 4147, 4076, 4007, 3938, 3879, 3821,
            3767, 3715, 3666, 3621, 3576, 3532, 3486, 3458, 3424, 3394, 3365,
            3334, 3311, 3285, 3266, 3249, 3228, 3210, 3199, 3179, 3159, 3148,
            3132, 3123, 3106, 3096, 3083, 3076, 3064, 3054, 3047, 3050, 3037,
            3035, 3027, 3031, 3018, 3014, 3004, 3001, 2990, 2985, 2985, 2984,
            2981, 2975, 2978, 2976, 2968, 2965, 2963, 2965, 2955, 2950, 2954,
            2953, 2949, 2948, 2944, 2939, 2945, 2943, 2942, 2936, 2936, 2939,
            2941, 2941, 2941, 2935, 2931, 2931, 2929, 2933, 2934, 2932, 2928,
            2928, 2924, 2922, 2917, 2924, 2922, 2922, 2923, 2923, 2922, 2929,
            2928, 2925, 2924, 2924, 2927, 2926, 2920, 2926, 2924, 2923, 2926,
            2922, 2924, 2921, 2925, 2920, 2920, 2923, 2918, 2917, 2915, 2920,
            2919, 2919, 2921, 2922, 2916, 2925, 2920, 2925, 2921, 2922, 2922,
            2922, 2921, 2925, 2924, 2922, 2922, 2920, 2920, 2918, 2921, 2920,
            2916, 2918, 2920, 2919, 2923, 2921, 2922, 2924, 2927, 2920, 2923,
            2921, 2919, 2921, 2920, 2920, 2920, 2925, 2920, 2916, 2915, 2916,
            2919, 2920, 2916, 2922, 2922, 2921, 2922, 2920, 2928, 2925, 2916,
            2919, 2919, 2923, 2918, 2919, 2922, 2922, 2922, 2926, 2917, 2919,
            2923, 2922, 2920, 2922, 2918, 2925, 2922, 2918, 2922, 2919, 2920,
            2924, 2914, 2917, 2916, 2917, 2914, 2908, 2913, 2913, 2913, 2913,
            2912, 2914, 2907, 2907, 2908, 2910, 2906, 2909, 2909, 2908, 2909,
            2907, 2912, 2906, 2906, 2907, 2905, 2903, 2902, 2910, 2905, 2905,
            2900, 2906, 2909, 2914, 2911, 2909, 2908, 2909, 2912, 2904, 2912,
            2908, 2907, 2905, 2901, 2904, 2900, 2902, 2901, 2904, 2904, 2895,
            2903, 2906, 2901, 2899, 2902, 2901, 2903, 2899, 2900, 2898, 2903,
            2900, 2899, 2900, 2900, 2898, 2902, 2902, 2900, 2902, 2903, 2893,
            2899, 2900, 2901, 2897, 2900, 2904, 2903, 2900, 2895, 2904, 2902,
            2902, 2902, 2903, 2899, 2901, 2901, 2905, 2900, 2892, 2903, 2898,
            2898, 2896, 2900, 2906, 2900, 2896, 2902, 2898, 2904, 2901, 2899,
            2903, 2900, 2897, 2896, 2902, 2901, 2902, 2897, 2901, 2899, 2906,
            2907, 2904, 2904, 2904, 2903, 2904, 2899, 2907, 2902, 2899, 2899,
            2899, 2898, 2904, 2905, 2904, 2899, 2898, 2905, 2904, 2897, 2900,
            2900, 2897, 2902, 2902, 2900, 2901, 2897, 2905, 2902, 2903, 2903,
            2902, 2900, 2901, 2904, 2904, 2897, 2900, 2901, 2904, 2903, 2902,
            2897, 2903, 2908, 2908, 2902, 2903, 2905, 2906, 2910, 2907, 2906,
            2907, 2907, 2908, 2909, 2911, 2911, 2912, 2905, 2910, 2909, 2912,
            2909, 2904, 2904, 2906, 2909, 2910, 2909, 2906, 2910, 2909, 2907,
            2904, 2908, 2912, 2907, 2908, 2910, 2908, 2912, 2907, 2908, 2913,
            2907, 2913, 2910, 2913, 2906, 2910, 2916, 2911, 2908, 2913, 2907,
            2904, 2911, 2910, 2914, 2913, 2908, 2907, 2912, 2914, 2906, 2911,
            2911, 2916, 2914, 2914, 2910, 2913, 2913, 2917, 2911, 2913, 2916,
            2917
        ]
        for i in range(len(samples)):
            self.assertEqual(newPulse['Samples'][i], samples[i])

        self.assertEqual(self.binFile.numberOfPulsesUnread,
                         self.binFile.totalNumberOfPulses - 1)

    def testGetSecondPulse(self):
        self.binFile.skipNextPulse()
        newPulse = self.binFile.readNextPulse()
        self.assertEqual(newPulse['Board'], 0)
        self.assertEqual(newPulse['Channel'], 0)
        self.assertEqual(newPulse['Time Stamp'], 1353249187)
        self.assertEqual(newPulse['Energy'], 97)
        self.assertEqual(newPulse['Energy Short'], 49)
        self.assertEqual(newPulse['Flags'], 16384)
        self.assertEqual(newPulse['Number of Samples'], 496)
        firstSample = 2907
        self.assertEqual(newPulse['Samples'][0], firstSample)
        lastSample = 2915
        self.assertEqual(newPulse['Samples'][-1], lastSample)

        self.assertEqual(self.binFile.numberOfPulsesUnread,
                         self.binFile.totalNumberOfPulses - 2)

    def testGetLastPulse(self):
        skipNumber = self.binFile.totalNumberOfPulses - 1
        self.binFile.skipNextNPulses(skipNumber)
        newPulse = self.binFile.readNextPulse()
        self.assertEqual(newPulse['Board'], 0)
        self.assertEqual(newPulse['Channel'], 0)
        self.assertEqual(newPulse['Time Stamp'], 600161252878229)
        self.assertEqual(newPulse['Energy'], 199)
        self.assertEqual(newPulse['Energy Short'], 103)
        self.assertEqual(newPulse['Flags'], 16384)
        self.assertEqual(newPulse['Number of Samples'], 496)
        firstSample = 2908
        self.assertEqual(newPulse['Samples'][0], firstSample)
        lastSample = 2911
        self.assertEqual(newPulse['Samples'][-1], lastSample)

        self.assertEqual(self.binFile.numberOfPulsesUnread, 0)
    
    def testRewind(self):
        skipNumber = self.binFile.totalNumberOfPulses
        self.binFile.skipNextNPulses(skipNumber)
        self.binFile.rewind()
        self.assertEqual(self.binFile.numberOfPulsesUnread, self.binFile.totalNumberOfPulses)

        newPulse = self.binFile.readNextPulse()
        self.assertEqual(newPulse['Board'], 0)
        self.assertEqual(newPulse['Channel'], 0)
        self.assertEqual(newPulse['Time Stamp'], 241636409)
        self.assertEqual(newPulse['Energy'], 297)
        self.assertEqual(newPulse['Energy Short'], 153)
        self.assertEqual(newPulse['Flags'], 16384)
        self.assertEqual(newPulse['Number of Samples'], 496)


class TestBinParserV2(unittest.TestCase):
    def setUp(self):
        self.binFile = WaveBinFile('test_data_CoMPASSv2/test.bin')

    def testGetCommonHeaderSuccess(self):
        self.assertEqual(self.binFile.versionNumber, 2)
        self.assertEqual(self.binFile.boardNumber, 0)
        self.assertEqual(self.binFile.channelNumber, 4)
        self.assertEqual(self.binFile._headersize, 25)
        self.assertEqual(self.binFile.numberOfSamplesPerPulse, 496)
        self.assertEqual(self.binFile.totalNumberOfPulses,
                         int((100 * 1024 * 1024) / (25 + 992)))

    def testGetFirstPulse(self):
        newPulse = self.binFile.readNextPulse()
        self.assertEqual(newPulse['Board'], 0)
        self.assertEqual(newPulse['Channel'], 4)
        self.assertEqual(newPulse['Time Stamp'], 1721147)
        self.assertEqual(newPulse['Energy'], 108)
        self.assertEqual(newPulse['Energy Short'], 141)
        self.assertEqual(newPulse['Flags'], 16448)
        self.assertEqual(newPulse['Waveform Code'], 1)
        self.assertEqual(newPulse['Number of Samples'], 496)
        samples = [
            2932, 2947, 2930, 2904, 2908, 2937, 2941, 2904, 2902, 2925, 2928,
            2917, 2901, 2910, 2928, 2925, 2903, 2893, 2924, 2936, 2916, 2877,
            2877, 2918, 2974, 3134, 3110, 3118, 3158, 3150, 3101, 3074, 3094,
            3107, 3096, 3063, 3059, 3074, 3084, 3079, 3030, 3027, 3055, 3073,
            3009, 2996, 3022, 3039, 3036, 2992, 2987, 3017, 3024, 3003, 2971,
            2979, 3023, 3011, 2978, 2945, 2975, 3019, 3001, 2958, 2947, 2984,
            3008, 2985, 2946, 2946, 2983, 2992, 2950, 2932, 2942, 2970, 2970,
            2936, 2918, 2943, 2964, 2955, 2929, 2927, 2963, 2969, 2940, 2921,
            2932, 2963, 2944, 2920, 2900, 2941, 2961, 2937, 2901, 2916, 2950,
            2955, 2908, 2903, 2937, 2958, 2950, 2920, 2910, 2942, 2957, 2956,
            2927, 2905, 2945, 2961, 2929, 2895, 2923, 2956, 2955, 2920, 2913,
            2940, 2966, 2941, 2918, 2926, 2939, 2957, 2945, 2922, 3208, 3425,
            3407, 3535, 3580, 3577, 3580, 3544, 3483, 3468, 3491, 3474, 3419,
            3381, 3383, 3404, 3390, 3331, 3298, 3313, 3321, 3308, 3248, 3244,
            3258, 3263, 3244, 3181, 3192, 3215, 3204, 3170, 3136, 3154, 3174,
            3148, 3111, 3114, 3140, 3127, 3101, 3090, 3085, 3099, 3106, 3074,
            3059, 3069, 3098, 3080, 3039, 3034, 3059, 3070, 3047, 3006, 3019,
            3053, 3056, 3024, 3005, 3022, 3043, 3010, 2984, 2998, 3018, 3027,
            3002, 2992, 2990, 3016, 3006, 2984, 2972, 2978, 3018, 2992, 2960,
            2946, 2983, 3001, 2962, 2948, 2960, 3000, 2975, 2953, 2964, 2969,
            2983, 2968, 2933, 2929, 2952, 2973, 2946, 2922, 2943, 2971, 2974,
            2940, 2919, 2953, 2975, 2959, 2933, 2923, 2944, 2965, 2952, 2925,
            2929, 2955, 2966, 2938, 2917, 2943, 2963, 2950, 2924, 2921, 2962,
            2945, 2918, 2904, 2938, 2964, 2946, 2913, 2895, 2916, 2965, 2938,
            2905, 2901, 2941, 2950, 2913, 2893, 2906, 2928, 2928, 2908, 2894,
            2912, 2921, 2937, 2907, 2895, 2915, 2933, 2897, 2889, 2915, 2930,
            2915, 2879, 2897, 2917, 2925, 2908, 2884, 2882, 2924, 2932, 2898,
            2874, 2900, 2942, 2919, 2876, 2876, 2911, 2930, 2917, 2877, 2878,
            2923, 2930, 2913, 2876, 2876, 2918, 2928, 2914, 2870, 2893, 2919,
            2925, 2884, 2880, 2898, 2914, 2905, 2880, 2889, 2920, 2914, 2881,
            2881, 2890, 2908, 2906, 2878, 2877, 2902, 2911, 2881, 2876, 2890,
            2909, 2903, 2870, 2864, 2891, 2920, 2896, 2870, 2870, 2897, 2920,
            2899, 2869, 2872, 2914, 2916, 2879, 2864, 2885, 2909, 2899, 2872,
            2874, 2900, 2907, 2887, 2876, 2881, 2904, 2922, 2873, 2867, 2899,
            2924, 2898, 2859, 2873, 2908, 2923, 2884, 2867, 2877, 2914, 2920,
            2887, 2874, 2898, 2918, 2907, 2865, 2864, 2897, 2912, 2897, 2872,
            2877, 2922, 2922, 2890, 2867, 2895, 2929, 2905, 2883, 2880, 2908,
            2923, 2904, 2881, 2884, 2922, 2922, 2898, 2879, 2904, 2932, 2920,
            2882, 2878, 2916, 2936, 2904, 2875, 2888, 2930, 2926, 2895, 2877,
            2899, 2930, 2925, 2889, 2887, 2918, 2943, 2929, 2880, 2893, 2942,
            2940, 2907, 2887, 2916, 2939, 2919, 2906, 2914, 2926, 2935, 2915,
            2879, 2905, 2925, 2923, 2899, 2883, 2906, 2950, 2923, 2886, 2875,
            2922, 2946, 2914, 2880, 2895, 2945, 2932, 2900, 2872, 2900, 2946,
            2935, 2901, 2876, 2911, 2934, 2923, 2885, 2882, 2917, 2931, 2920,
            2895, 2908, 2934, 2929, 2902, 2890, 2911, 2933, 2908, 2901, 2900,
            2936
        ]
        for i in range(len(samples)):
            self.assertEqual(newPulse['Samples'][i], samples[i])

        self.assertEqual(self.binFile.numberOfPulsesUnread,
                         self.binFile.totalNumberOfPulses - 1)

    def testGetSecondPulse(self):
        self.binFile.skipNextPulse()
        newPulse = self.binFile.readNextPulse()
        self.assertEqual(newPulse['Board'], 0)
        self.assertEqual(newPulse['Channel'], 4)
        self.assertEqual(newPulse['Time Stamp'], 3213109)
        self.assertEqual(newPulse['Energy'], 143)
        self.assertEqual(newPulse['Energy Short'], 191)
        self.assertEqual(newPulse['Flags'], 16448)
        self.assertEqual(newPulse['Number of Samples'], 496)
        firstSample = 2889
        self.assertEqual(newPulse['Samples'][0], firstSample)
        lastSample = 2899
        self.assertEqual(newPulse['Samples'][-1], lastSample)

        self.assertEqual(self.binFile.numberOfPulsesUnread,
                         self.binFile.totalNumberOfPulses - 2)

    def testGetLastPulse(self):
        skipNumber = self.binFile.totalNumberOfPulses - 1
        self.binFile.skipNextNPulses(skipNumber)
        newPulse = self.binFile.readNextPulse()
        self.assertEqual(newPulse['Board'], 0)
        self.assertEqual(newPulse['Channel'], 4)
        self.assertEqual(newPulse['Time Stamp'], 1400692490936)
        self.assertEqual(newPulse['Energy'], 4095)
        self.assertEqual(newPulse['Energy Short'], 4095)
        self.assertEqual(newPulse['Flags'], 16512)
        self.assertEqual(newPulse['Number of Samples'], 496)
        firstSample = 2887
        self.assertEqual(newPulse['Samples'][0], firstSample)
        lastSample = 2885
        self.assertEqual(newPulse['Samples'][-1], lastSample)

        self.assertEqual(self.binFile.numberOfPulsesUnread, 0)
    
    def testRewind(self):
        skipNumber = self.binFile.totalNumberOfPulses
        self.binFile.skipNextNPulses(skipNumber)
        self.binFile.rewind()
        self.assertEqual(self.binFile.numberOfPulsesUnread, self.binFile.totalNumberOfPulses)

        newPulse = self.binFile.readNextPulse()
        self.assertEqual(newPulse['Board'], 0)
        self.assertEqual(newPulse['Channel'], 4)
        self.assertEqual(newPulse['Time Stamp'], 1721147)
        self.assertEqual(newPulse['Energy'], 108)
        self.assertEqual(newPulse['Energy Short'], 141)
        self.assertEqual(newPulse['Flags'], 16448)
        self.assertEqual(newPulse['Waveform Code'], 1)
        self.assertEqual(newPulse['Number of Samples'], 496)

        samples = [
            2932, 2947, 2930, 2904, 2908, 2937, 2941, 2904, 2902, 2925, 2928,
            2917, 2901, 2910, 2928, 2925, 2903, 2893, 2924, 2936, 2916, 2877,
            2877, 2918, 2974, 3134, 3110, 3118, 3158, 3150, 3101, 3074, 3094,
            3107, 3096, 3063, 3059, 3074, 3084, 3079, 3030, 3027, 3055, 3073,
            3009, 2996, 3022, 3039, 3036, 2992, 2987, 3017, 3024, 3003, 2971,
            2979, 3023, 3011, 2978, 2945, 2975, 3019, 3001, 2958, 2947, 2984,
            3008, 2985, 2946, 2946, 2983, 2992, 2950, 2932, 2942, 2970, 2970,
            2936, 2918, 2943, 2964, 2955, 2929, 2927, 2963, 2969, 2940, 2921,
            2932, 2963, 2944, 2920, 2900, 2941, 2961, 2937, 2901, 2916, 2950,
            2955, 2908, 2903, 2937, 2958, 2950, 2920, 2910, 2942, 2957, 2956,
            2927, 2905, 2945, 2961, 2929, 2895, 2923, 2956, 2955, 2920, 2913,
            2940, 2966, 2941, 2918, 2926, 2939, 2957, 2945, 2922, 3208, 3425,
            3407, 3535, 3580, 3577, 3580, 3544, 3483, 3468, 3491, 3474, 3419,
            3381, 3383, 3404, 3390, 3331, 3298, 3313, 3321, 3308, 3248, 3244,
            3258, 3263, 3244, 3181, 3192, 3215, 3204, 3170, 3136, 3154, 3174,
            3148, 3111, 3114, 3140, 3127, 3101, 3090, 3085, 3099, 3106, 3074,
            3059, 3069, 3098, 3080, 3039, 3034, 3059, 3070, 3047, 3006, 3019,
            3053, 3056, 3024, 3005, 3022, 3043, 3010, 2984, 2998, 3018, 3027,
            3002, 2992, 2990, 3016, 3006, 2984, 2972, 2978, 3018, 2992, 2960,
            2946, 2983, 3001, 2962, 2948, 2960, 3000, 2975, 2953, 2964, 2969,
            2983, 2968, 2933, 2929, 2952, 2973, 2946, 2922, 2943, 2971, 2974,
            2940, 2919, 2953, 2975, 2959, 2933, 2923, 2944, 2965, 2952, 2925,
            2929, 2955, 2966, 2938, 2917, 2943, 2963, 2950, 2924, 2921, 2962,
            2945, 2918, 2904, 2938, 2964, 2946, 2913, 2895, 2916, 2965, 2938,
            2905, 2901, 2941, 2950, 2913, 2893, 2906, 2928, 2928, 2908, 2894,
            2912, 2921, 2937, 2907, 2895, 2915, 2933, 2897, 2889, 2915, 2930,
            2915, 2879, 2897, 2917, 2925, 2908, 2884, 2882, 2924, 2932, 2898,
            2874, 2900, 2942, 2919, 2876, 2876, 2911, 2930, 2917, 2877, 2878,
            2923, 2930, 2913, 2876, 2876, 2918, 2928, 2914, 2870, 2893, 2919,
            2925, 2884, 2880, 2898, 2914, 2905, 2880, 2889, 2920, 2914, 2881,
            2881, 2890, 2908, 2906, 2878, 2877, 2902, 2911, 2881, 2876, 2890,
            2909, 2903, 2870, 2864, 2891, 2920, 2896, 2870, 2870, 2897, 2920,
            2899, 2869, 2872, 2914, 2916, 2879, 2864, 2885, 2909, 2899, 2872,
            2874, 2900, 2907, 2887, 2876, 2881, 2904, 2922, 2873, 2867, 2899,
            2924, 2898, 2859, 2873, 2908, 2923, 2884, 2867, 2877, 2914, 2920,
            2887, 2874, 2898, 2918, 2907, 2865, 2864, 2897, 2912, 2897, 2872,
            2877, 2922, 2922, 2890, 2867, 2895, 2929, 2905, 2883, 2880, 2908,
            2923, 2904, 2881, 2884, 2922, 2922, 2898, 2879, 2904, 2932, 2920,
            2882, 2878, 2916, 2936, 2904, 2875, 2888, 2930, 2926, 2895, 2877,
            2899, 2930, 2925, 2889, 2887, 2918, 2943, 2929, 2880, 2893, 2942,
            2940, 2907, 2887, 2916, 2939, 2919, 2906, 2914, 2926, 2935, 2915,
            2879, 2905, 2925, 2923, 2899, 2883, 2906, 2950, 2923, 2886, 2875,
            2922, 2946, 2914, 2880, 2895, 2945, 2932, 2900, 2872, 2900, 2946,
            2935, 2901, 2876, 2911, 2934, 2923, 2885, 2882, 2917, 2931, 2920,
            2895, 2908, 2934, 2929, 2902, 2890, 2911, 2933, 2908, 2901, 2900,
            2936
        ]
        for i in range(len(samples)):
            self.assertEqual(newPulse['Samples'][i], samples[i])

        self.assertEqual(self.binFile.numberOfPulsesUnread,
                         self.binFile.totalNumberOfPulses - 1)


if __name__ == '__main__':
    unittest.main()